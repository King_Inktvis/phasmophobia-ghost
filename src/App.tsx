import React from 'react';
import {
    Container,
    createMuiTheme,
    CssBaseline,
    FormLabel,
    Radio,
    Table,
    TableCell, TableHead,
    TableRow,
    ThemeProvider,
} from '@material-ui/core';

const darkTheme = createMuiTheme({
    palette: {
        type: 'dark',
    },
});

const Evidence = {
    EMFLevel5: 'EMF Level 5',
    GhostWriting: 'Ghost Writing',
    Fingerprints: 'Fingerprints',
    SpiritBox: 'Spirit Box',
    FreezingTemperatures: 'Freezing Temperatures',
    GhostOrb: 'Ghost Orb',
};

const evidenceList = [Evidence.EMFLevel5, Evidence.GhostWriting, Evidence.Fingerprints, Evidence.SpiritBox, Evidence.FreezingTemperatures, Evidence.GhostOrb];

interface Ghost {
    name: string;
    evidence: string[];
}

const ghosts: Ghost[] = [
    {
        name: 'Banshee',
        evidence: [Evidence.EMFLevel5, Evidence.Fingerprints, Evidence.FreezingTemperatures],
    },
    {
        name: 'Demon',
        evidence: [Evidence.GhostWriting, Evidence.SpiritBox, Evidence.FreezingTemperatures],
    },
    {
        name: 'Jinn',
        evidence: [Evidence.EMFLevel5, Evidence.SpiritBox, Evidence.GhostOrb],
    },
    {
        name: 'Mare',
        evidence: [Evidence.SpiritBox, Evidence.GhostOrb, Evidence.FreezingTemperatures],
    },
    {
        name: 'Oni',
        evidence: [Evidence.EMFLevel5, Evidence.GhostWriting, Evidence.SpiritBox],
    },
    {
        name: 'Phantom',
        evidence: [Evidence.EMFLevel5, Evidence.FreezingTemperatures, Evidence.GhostOrb],
    },
    {
        name: 'Poltergeist',
        evidence: [Evidence.Fingerprints, Evidence.SpiritBox, Evidence.GhostOrb],
    },
    {
        name: 'Revenant',
        evidence: [Evidence.EMFLevel5, Evidence.GhostWriting, Evidence.Fingerprints],
    },
    {
        name: 'Shade',
        evidence: [Evidence.EMFLevel5, Evidence.GhostWriting, Evidence.GhostOrb],
    },
    {
        name: 'Spirit',
        evidence: [Evidence.GhostWriting, Evidence.Fingerprints, Evidence.SpiritBox],
    },
    {
        name: 'Wraith',
        evidence: [Evidence.Fingerprints, Evidence.SpiritBox, Evidence.FreezingTemperatures],
    },
    {
        name: 'Yurei',
        evidence: [Evidence.GhostWriting, Evidence.FreezingTemperatures, Evidence.GhostOrb],
    },
];

interface EvidenceCollection {
    type: string;
    value: string;
}

function App(): JSX.Element {
    const tmp = evidenceList.map(eve => ({ type: eve, value: 'unknown' }));
    const [value, setValue] = React.useState(tmp);

    function handleChange(type: string, event: React.ChangeEvent<HTMLInputElement>): void {
        const cast = event.target as unknown as HTMLInputElement;
        const found = value.find(element => element.type === type);
        let newValue: EvidenceCollection[];
        if (found) {
            newValue = value.map(element => {
                if (element.type === type) {
                    return { type, value: cast.value };
                }else {
                    return element;
                }
            });
        }else {
            newValue = [...value];
            newValue.push({ type, value: cast.value });
        }
        setValue(newValue);
    }

    const evidenceSelectors = evidenceList.map(evidence => {
        const formValue = value.find(e => e.type === evidence)?.value;
        return (
            <React.Fragment key={evidence}>
                <TableRow>
                    <TableCell>{evidence}</TableCell>
                    <TableCell><Radio value='evidence' checked={formValue === 'evidence'}
                        onChange={(e): void => handleChange(evidence, e)}/></TableCell>
                    <TableCell><Radio value='unknown' checked={formValue === 'unknown'}
                        onChange={(e): void => handleChange(evidence, e)}/></TableCell>
                    <TableCell><Radio value='no-evidence' checked={formValue === 'no-evidence'}
                        onChange={(e): void => handleChange(evidence, e)}/></TableCell>
                </TableRow>
            </React.Fragment>
        );
    });

    const mustHave = value.filter(v => v.value === 'evidence').map(tmp => tmp.type);
    const dontHave = value.filter(v => v.value === 'no-evidence').map(tmp => tmp.type);
    const options = ghosts.filter(ghost => {
        for (let i = 0; i < mustHave.length; ++i) {
            if (!ghost.evidence.find(e => e === mustHave[i])) {
                return false;
            }
        }
        for (let i = 0; i < dontHave.length; ++i) {
            if (ghost.evidence.find(e => e === dontHave[i])) {
                return false;
            }
        }
        return true;
    });
    return (
        <div style={{ width: '100%', marginTop: 10 }}>
            <ThemeProvider theme={darkTheme}>
                <CssBaseline/>
                <Container maxWidth='xs'>
                    <FormLabel>Evidence</FormLabel>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell style={{ textAlign: 'center' }}>✓</TableCell>
                                <TableCell style={{ textAlign: 'center' }}>?</TableCell>
                                <TableCell style={{ textAlign: 'center' }}>x</TableCell>
                            </TableRow>
                            {evidenceSelectors}
                        </TableHead>
                    </Table><br/>
                    <FormLabel>Possibilities</FormLabel>
                    <Table>
                        <TableHead>
                            {options.map(o => <TableRow key={o.name}>
                                <TableCell>{o.name}</TableCell>
                                {
                                    o.evidence.filter(ev => !mustHave.find(mh => mh === ev))
                                        .map(v => <TableCell key={v}>{v}</TableCell>)
                                }
                            </TableRow>)
                            }
                        </TableHead>
                    </Table>
                </Container>
            </ThemeProvider>
        </div>
    );
}

export default App;
